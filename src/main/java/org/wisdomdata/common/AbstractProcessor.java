package org.wisdomdata.common;

import java.util.List;

import org.wisdomdata.framework.Processor;

public abstract class AbstractProcessor implements Processor {
	public static final String TARGET_URL = "TARGET_URL";
	//是否将这一层次产生的表，生成一个新表
	private boolean createNewResultTable = false;
	public boolean isCreateNewResultTable() {
		return createNewResultTable;
	}

	public void setCreateNewResultTable(boolean createNewResultTable) {
		this.createNewResultTable = createNewResultTable;
	}

	
	/**
	 * 是否需要立即保存
	 * */
	private boolean needStoreDone = false;
	
	public boolean isNeedStoreDone() {
		return needStoreDone;
	}

	public void setNeedStoreDone(boolean needStoreDone) {
		this.needStoreDone = needStoreDone;
	}


	/**
	 * 产生一个新表的名字
	 * */
	private String tableName;
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	boolean needStoreTargetUri = true;
	
	
	public boolean isNeedStoreTargetUri() {
		return needStoreTargetUri;
	}

	public void setNeedStoreTargetUri(boolean needStoreTargetUri) {
		this.needStoreTargetUri = needStoreTargetUri;
	}

	//紧接着的后续处理器，这个处理器之前如果没有出现点击任务或者跳转任务，那么继续上个页面处理
	private List<Processor> childProcessors;
	public List<Processor> getChildProcessors() {
		return childProcessors;
	}

	public void setChildProcessors(List<Processor> childProcessors) {
		this.childProcessors = childProcessors;
	}
	
}
