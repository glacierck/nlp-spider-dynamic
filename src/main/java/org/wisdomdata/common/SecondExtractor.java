package org.wisdomdata.common;

import org.wisdomdata.framework.Extractor;

public abstract class SecondExtractor implements Extractor{
	private String sourceText;
	public String getSourceText() {
		return sourceText;
	}
	public void setSourceText(String sourceText) {
		this.sourceText = sourceText;
	}

	private String extractResult;

	public String getExtractResult() {
		return extractResult;
	}

	public void setExtractResult(String extractResult) {
		this.extractResult = extractResult;
	}
	
	
	public void extract() {
		//尝试三次
				int mark = 0;
				while(true) {
					mark++;
					try {
						if (prepareExtract()) {
							innerExtract();
							quitExtract();
						}
					} catch (Exception e) {
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
						if (3 <= mark) {
							break;
						}
						else 
							continue;
					}	
					break;
				} 
				
	}
	public boolean prepareExtract() {
		return true;
	}

	public void quitExtract() {
		// TODO Auto-generated method stub
		
	}
	
}
