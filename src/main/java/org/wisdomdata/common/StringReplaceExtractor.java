package org.wisdomdata.common;

import org.mortbay.util.StringUtil;

/**
 * 替换抽取器，通过这个抽取器之后，将对需要替换的字符串替换成指定的字符串
 * @author Clebeg
 * @time 2014-12-07
 * @version v1
 * */
public class StringReplaceExtractor extends SecondExtractor {

	private String target;
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	
	private String replace;
	public String getReplace() {
		return replace;
	}
	public void setReplace(String replace) {
		this.replace = replace;
	}
	
	
	public void innerExtract() {
		String result = StringUtil.replace(this.getSourceText(), target, replace);
		this.setExtractResult(result);
	}

}
