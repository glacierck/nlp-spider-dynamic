package org.wisdomdata.selenium.crawler;

import org.openqa.selenium.WebDriver;
import org.wisdomdata.selenium.SeleniumSpider;

public class DynamicSeleniumSpider extends SeleniumSpider{
	/**
	 * DynamicSeleniumSpider 建议 webDriver 可以是用 火狐或者谷歌
	 * 它的特点是：
	 * 1）可以抓取网页上的动态信息，比如js执行之后的信息
	 * 2）抓取速度比较慢
	 * */
	public void crawl() {
		((WebDriver) this.getSearchContext()).get(this.getUri());
	}
	public void action() {
		this.crawl();
	}

}
