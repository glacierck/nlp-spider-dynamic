package org.wisdomdata.selenium.extractor;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.wisdomdata.selenium.SeleniumStringExtractor;

public class HTMLExtractorByCSSPath extends SeleniumStringExtractor{
	private String csspath;

	public String getCsspath() {
		return csspath;
	}
	public void setCsspath(String csspath) {
		this.csspath = csspath;
	}

	public void innerExtract() throws NoSuchElementException{
		
		String result = "";
		WebElement element = this.getSearchContext().findElement(By.cssSelector(this.getCsspath()));
		
		result = element.getAttribute("outerHTML");
	
		this.setExtractResult(result);
	}

}
