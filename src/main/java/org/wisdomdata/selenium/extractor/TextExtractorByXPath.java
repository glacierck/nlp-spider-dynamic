package org.wisdomdata.selenium.extractor;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.wisdomdata.selenium.SeleniumStringExtractor;

public class TextExtractorByXPath extends SeleniumStringExtractor{
	
	private String xpath;
	public String getXpath() {
		return xpath;
	}
	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	public void innerExtract() throws NoSuchElementException {
		
		String result = "";
		//首先判定是否存在这个元素，不存在就
		WebElement element = this.getSearchContext().findElement(By.xpath(getXpath()));
		result = element.getText();

		this.setExtractResult(result);
	}

}
