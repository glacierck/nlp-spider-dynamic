package org.wisdomdata.selenium.processor;


import org.wisdomdata.framework.Processor;


public class SeleniumClickNormalProcessor extends SeleniumActionProcessor {
	public void innerProcess() {
		this.commonExtract();
		
		//先抽取东西，然后点击
		//因为如果先点击再抽取东西，子处理过程也可以处理
		this.doAction();
		
		if (this.getChildProcessors() != null && this.getChildProcessors().size() > 0) {
			for (Processor processor : this.getChildProcessors()) {
				processor.process();
			}	
		}
	}

}
