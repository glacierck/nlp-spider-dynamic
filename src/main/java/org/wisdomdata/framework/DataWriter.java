package org.wisdomdata.framework;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface DataWriter {
	void writeTables(Map<String, Map<String, List<String>>> ts, String rootDir, String suffix) throws IOException;
}
