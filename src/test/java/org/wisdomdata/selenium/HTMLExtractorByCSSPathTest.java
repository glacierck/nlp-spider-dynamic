package org.wisdomdata.selenium;

import org.openqa.selenium.WebDriver;
import org.wisdomdata.selenium.extractor.HTMLExtractorByCSSPath;

import junit.framework.TestCase;

public class HTMLExtractorByCSSPathTest extends TestCase {
	public String extractTest() {
		System.setProperty ("webdriver.firefox.bin" , "D:/Program Files (x86)/Mozilla Firefox/firefox.exe" );
		HTMLExtractorByCSSPath hebx = new HTMLExtractorByCSSPath();
		//WebDriver webDriver = new HtmlUnitDriver();
		WebDriver webDriver = new org.openqa.selenium.firefox.FirefoxDriver();
		webDriver.get("http://news.sina.com.cn/c/2014-12-06/023031252624.shtml");
		hebx.setSearchContext(webDriver);
		hebx.setCsspath("#artibody > p.article-editor");
		hebx.extract();
		String result = hebx.getExtractResult();
		System.out.println(result);
		return result;
	}
}
