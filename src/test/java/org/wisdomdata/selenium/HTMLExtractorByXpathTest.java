package org.wisdomdata.selenium;

import org.openqa.selenium.WebDriver;
import org.wisdomdata.selenium.extractor.HTMLExtractorByXPath;

import junit.framework.TestCase;

public class HTMLExtractorByXpathTest extends TestCase {
	public String extractTest() {
		System.setProperty ("webdriver.firefox.bin" , "D:/Program Files (x86)/Mozilla Firefox/firefox.exe" );
		HTMLExtractorByXPath hebx = new HTMLExtractorByXPath();
		//WebDriver webDriver = new HtmlUnitDriver();
		WebDriver webDriver = new org.openqa.selenium.firefox.FirefoxDriver();
		webDriver.get("http://news.sina.com.cn/c/2014-12-06/023031252624.shtml");
		hebx.setSearchContext(webDriver);
		hebx.setXpath("//*[@id=\"artibody\"]/p[31]");
		hebx.extract();
		String results = hebx.getExtractResult();
		System.out.println(results);
		webDriver.quit();
		return results;
	}
}
