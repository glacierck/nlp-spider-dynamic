package org.wisdomdata.common;

import junit.framework.TestCase;

public class RegExpExtractorTest extends TestCase {
	public void innerExtractTest() {
		RegExpExtractor extractor  = new RegExpExtractor();
		extractor.setSourceText("1/4");
		extractor.setPatternString("\\d{1,3}/(\\d{1,3})");
		extractor.setGroupId(1);
		extractor.extract();
		System.out.println(extractor.getExtractResult());
	}
}
