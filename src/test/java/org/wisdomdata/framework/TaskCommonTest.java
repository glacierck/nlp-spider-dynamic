package org.wisdomdata.framework;

import junit.framework.TestCase;

public class TaskCommonTest extends TestCase {
	public void getTableNumberTest() {
		TaskCommon taskCommon = new TaskCommon();
		taskCommon.setTaskRootFile("tasks/taobao");
		taskCommon.setTableMetaData("tables.metadata");
		int number = taskCommon.getTableNumber("DealRecord");
		System.out.println(number);
	}
	
	public void setTableNumberTest() {
		TaskCommon taskCommon = new TaskCommon();
		taskCommon.setTableMetaData("tables.metadata");
		taskCommon.setTableNumber("DealRecord", 400);
		int number = taskCommon.getTableNumber("DealRecord");
		System.out.println(number);
	}
}
